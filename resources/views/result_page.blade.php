@extends('index')

@section('content')

    @if(isset($data) && count($data))
        @foreach($data as $item)
            <p>Last Update: {{date('d/m/Y', strtotime($item['last_update']))}}</p>
            <table border="1">
                <thead>
                    <tr>
                        <td>Name</td>
                        <td>Unit</td>
                        <td>Currency Code</td>
                        <td>Country</td>
                        <td>Rate</td>
                        <td>Change</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($item['currency'] as $currency)
                        <tr>
                            <td>{{$currency['name']}}</td>
                            <td>{{$currency['unit']}}</td>
                            <td>{{$currency['currencycode']}}</td>
                            <td>{{$currency['country']}}</td>
                            <td>{{$currency['rate']}}</td>
                            <td>{{$currency['change'] > 0 ? '+' . $currency['change'] : $currency['change']}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <hr>
        @endforeach

        {{--Upload files--}}
        <p>Upload files</p>
        <div>
            @if(isset($jsonFileName))
                <button>
                    <a href="/download?filename={{$jsonFileName}}">
                        JSON
                    </a>
                </button>
            @endif
            @if(isset($csvFileName))
                <button>
                    <a href="/download?filename={{$csvFileName}}">
                        CSV
                    </a>
                </button>
            @endif
            @if(isset($xmlSimpleFileName))
                <button>
                    <a href="/download?filename={{$xmlSimpleFileName}}">
                        XML (SimpleXML)
                    </a>
                </button>
            @endif
            @if(isset($xmlWriterFileName))
                <button>
                    <a href="/download?filename={{$xmlWriterFileName}}">
                        XML (XMLWriter)
                    </a>
                </button>
            @endif
        </div>

    @else
        <p color="red">Error</p>
    @endif



@endsection