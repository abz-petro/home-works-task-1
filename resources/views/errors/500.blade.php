<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Error</title>
</head>
<body>
    <h2 style="color:red;">{!! $exception->getMessage() !!}</h2>
    <a href="{{url()->previous()}}">back</a>
</body>
</html>