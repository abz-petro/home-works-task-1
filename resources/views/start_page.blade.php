@extends('index')

@section('content')
    @if(session('error'))
        <p style="color:red; text-align: center;">
            {{session('error')}}
        </p>
    @endif
    <center style="margin-top: 10rem;">
        <form action="/uploadFile" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}

            <input type="file" id="uploadFile" name="file">

            @if(isset($parseMethods))
                <div style="display: inline-block;">
                    <strong>Select parse method:&nbsp;&nbsp;&nbsp;</strong>
                    |
                    @foreach($parseMethods as $key => $method)
                        <label for="{{$method}}">{{$method}}</label>
                        <input type="radio" id="{{$method}}" name="parseMethod" value="{{$method}}" {{$key == 0 ? 'checked' : ''}}>
                        |
                    @endforeach
                </div>
            @endif

            <input type="submit" name="sendFile" value="Send File" style="margin-left:2rem;">

        </form>
    </center>

@endsection