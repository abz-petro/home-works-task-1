<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\Parser;
use App\Classes\Exceptions\FileException;

class IndexController extends Controller
{
    public function showStartPage()
    {
        $parseMethods = Parser::AVAILABLE_PARSE_METHODS;

        return view('start_page', compact('parseMethods'));
    }

    public function uploadFile(Request $request)
    {
        $parseMethod = $request->input('parseMethod');
        $file = $request->file('file');
        
        //parse file and write parsing data to $parser->parsingData
        $parser = new Parser();

        try {
            $parser->parse($file, $parseMethod);
            // Update data
            $parser->updateParsingData();
        } catch (FileException $e) {
            $error = 'Can\'t parse file and update data: ' . $e->getMessage();
            return back()->with('error', $error);
        }

        $data = $parser->getParsingData();

        try {
            // create new files from parsing data
            $parser->saveAs(storage_path('app/public/currency.json'), 'json');
            $parser->saveAs(storage_path('app/public/currency.csv'), 'csv');
            $parser->saveAs(storage_path('app/public/currency_simplexml.xml'), 'xml1');
            $parser->saveAs(storage_path('app/public/currency_xmlwriter.xml'), 'xml2');
        } catch (FileException $e) {
            $error = 'Can\'t save files: ' . $e->getMessage();
            return back()->with('error', $error);
        }

        $jsonFileName = 'currency.json';
        $csvFileName = 'currency.csv';
        $xmlSimpleFileName = 'currency_simplexml.xml';
        $xmlWriterFileName = 'currency_xmlwriter.xml';

        return view(
            'result_page',
            compact('data', 'jsonFileName', 'csvFileName', 'xmlSimpleFileName', 'xmlWriterFileName')
        );
    }

    public function downloadFile(Request $request)
    {
        return \Storage::download($request->input('filename'));
    }
}
