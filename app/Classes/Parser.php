<?php

namespace App\Classes;

use App\Classes\Exceptions\FileException;
use \Illuminate\Http\UploadedFile;

class Parser
{
    const AVAILABLE_FILE_FORMATS = ['json', 'xml', 'csv'];

    const AVAILABLE_PARSE_METHODS = ['csv', 'json', 'xml1', 'xml2'];

    /**
     * @var array
     */
    private $parsingData = [];

    /**
     * @var \App\Classes\FileHandler\AbstractFileHandler
     */
    private $fileHandler;

    /**
     * @var array
     */
    private $itemSchema = ['last_update', 'currency'];

    /**
     * @var array
     */
    private $currencySchema = ['name', 'unit', 'currencycode', 'country', 'rate', 'change'];

    public function getParsingData() :array
    {
        return $this->parsingData;
    }

    /**
     * @throws FileException
     */
    public function parse(UploadedFile $file, string $method) :self
    {
        $filePath = $file->getRealPath();

        if (!$this->validateFormatFile($file->getClientOriginalExtension(), $method)) {
            throw new FileException('Invalid format file or parse method');
        }

        $this->setFileHandler($method);

        $parsingData = $this->fileHandler->parseFile($filePath);

        if (!count($parsingData)) {
            throw new FileException('Can\'t parse file');
        }
        // Validate data scheme every item
        foreach ($parsingData as $item) {
            if (!$this->validateSchemaItem($item)) {
                throw new FileException('Invalid schema');
            }
        }
        // Validate every item by format data
        foreach ($parsingData as $item) {
            if (!$this->validateDataItem($item)) {
                throw new FileException('Invalid data format');
            }
        }

        $this->parsingData = $parsingData;

        return $this;
    }

    public function updateParsingData() :bool
    {
        $jsonRates = file_get_contents(database_path('/staticData/rates.json'));
        $rates = json_decode($jsonRates, true);

        $parsingData = $this->parsingData;
        $updatedData = [];
        foreach ($parsingData as $index => $item) {
            // update date
            $newItem['last_update'] = date('Y-m-d');

            foreach ($item['currency'] as $currencyIndex => $currency) {
                $newCurrency = $currency;
                // get currency rate from array 'rates
                $rate = $rates[$currency['currencycode']];
                // Set new rate
                $newCurrency['rate'] = $rate;
                // calculate new change = old rate - new rate
                $newCurrency['change'] = $currency['rate'] - $newCurrency['rate'];
                // add new currency to new item
                $newItem['currency'][$currencyIndex] = $newCurrency;
            }

            $updatedData[$index] = $newItem;
        }

        $this->parsingData = $updatedData;

        return true;
    }

    /**
     * @throws FileException
     */
    public function saveAs(string $filePath, string $method) :string
    {
        try {
            $this->setFileHandler($method);
        } catch (FileException $e) {
            throw new FileException($e->getMessage());
        }

        $this->fileHandler->saveAsFile($filePath, $this->parsingData);

        return $filePath;
    }

    /**
     * @throws FileException
     */
    private function setFileHandler(string $method) :void
    {
        $handler = 'App\Classes\FileHandler\\';
        switch ($method) {
            case 'json':
                $handler .= 'Json';
                break;
            case 'csv':
                $handler .= 'Csv';
                break;
            case 'xml1':
                $handler .= 'XmlSimple';
                break;
            case 'xml2':
                $handler .= 'XmlReaderWriter';
                break;
            default:
                throw new FileException('Not found method');
        }

        $this->fileHandler = new $handler();
    }

    private function validateFormatFile(string $fileExt, string $method) :bool
    {
        if (!in_array($fileExt, self::AVAILABLE_FILE_FORMATS)) {
            return false;
        }

        if ($method === 'xml1' || $method == 'xml2') {
            return $fileExt === 'xml';
        } else {
            return $fileExt === $method;
        }
    }

    private function validateSchemaItem(array $item) :bool
    {
        // check if keys item = default keys
        if ($this->itemSchema !== array_keys($item)) {
            return false;
        }
        // Check if item['currency'] is array
        if (!is_array($item['currency'])) {
            return false;
        }
        // check if keys item[currency] = default keys ($this->currencySchema)
        foreach ($item['currency'] as $currency) {
            if (!is_array($currency) || $this->currencySchema !== array_keys($currency)) {
                return false;
            }
        }

        return true;
    }

    private function validateDataItem(array $item) :bool
    {
        // check last_upload
        if (!date_create_from_format('Y-m-d', $item['last_update'])) {
            return false;
        }

        if (!isset($item['currency'])) {
            return false;
        }

        foreach ($item['currency'] as $el) {
            // check name
            if (!preg_match('/^[A-Za-z]*$/', $el['name'])) {
                return false;
            }
            // check unit
            if (filter_var($el['unit'], FILTER_VALIDATE_INT) === false) {
                return false;
            }
            // check currency
            $json = file_get_contents(database_path('/staticData/currencies.json'));
            $listCurrencies = array_keys(json_decode($json, true));
            if (!in_array($el['currencycode'], $listCurrencies)) {
                return false;
            }
            // check country
            $json = file_get_contents(database_path('/staticData/countries.json'));
            $listCountries = array_pluck(json_decode($json, true), 'name');
            if (!in_array($el['country'], $listCountries)) {
                return false;
            }
            //check rate
            if (filter_var($el['rate'], FILTER_VALIDATE_FLOAT) === false || $el['rate'] <= 0) {
                return false;
            }
            // check change
            if (filter_var($el['change'], FILTER_VALIDATE_FLOAT) === false) {
                return false;
            }
        }

        return true;
    }
}
