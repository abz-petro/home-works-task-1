<?php

namespace App\Classes\FileHandler;

abstract class AbstractFileHandler
{
    abstract public function parseFile(string $filePath) :array;

    abstract public function saveAsFile(string $filePath, array $array) :void;

    protected function isAssocArray(array $array) :bool
    {
        return array_keys($array) !== range(0, count($array) - 1);
    }
}
