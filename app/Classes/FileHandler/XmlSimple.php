<?php

namespace App\Classes\FileHandler;

use App\Classes\Exceptions\FileException;

class XmlSimple extends AbstractFileHandler
{
    /**
     * @throws FileException
     */
    public function parseFile(string $filePath) :array
    {
        // Create xmlObject and convert his to array
        try {
            $result = simplexml_load_file($filePath);
        } catch (\Exception $e) {
            $message = 'Invalid file structure';
            throw new FileException($message);
        }

        $assocArray = $this->toAssocArray($result);
        
        $indexArray = $this->toIndexArray($assocArray);
        
        return $indexArray;
    }

    public function saveAsFile(string $filePath, array $array) :void
    {
        $result = $this->arrayToXmlSimple($array);

        file_put_contents($filePath, $result);
    }

    /**
     * @param \SimpleXMLElement|array $array
     */
    private function toAssocArray($array) :array
    {
        $result = [];

        foreach ((array)$array as $index => $node) {
            if (is_object($node) || is_array($node)) {
                $result[$index] = $this->toAssocArray(($node));
            } else {
                $result[$index] = $node;
            }
        }

        return $result;
    }

    private function toIndexArray(array $array) :array
    {
        if (count($array) === 1) {
            $array = array_first($array);
        } else {
            $array = [$array];
        }

        $result = [];
        foreach ($array as $key => $item) {
            $newItem = $item;
            if (isset($item['currency']) && $this->isAssocArray($item['currency'])) {
                $newItem['currency'] = [$item['currency']];
            }
            $result[] = $newItem;
        }

        return $result;
    }

    private function arrayToXmlSimple(array $array, string $rootElement = null, \SimpleXMLElement $xml = null) :string
    {
        if ($xml === null) {
            $xml = new \SimpleXMLElement($rootElement !== null ? $rootElement : '<root/>');
        }

        foreach ($array as $key => $value) {
            $newKey = $key;
            if (is_array($value)) { //nested array
                if (is_numeric($newKey) && $rootElement === null) {
                    $newKey = 'item';
                }
                if (is_numeric($newKey)) {
                    $newKey = 'currency';
                }
                $this->arrayToXmlSimple($value, $newKey, $xml->addChild($newKey));
            } else {
                $xml->addChild($newKey, $value);
            }
        }

        return $xml->asXML();
    }
}
