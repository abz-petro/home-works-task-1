<?php

namespace App\Classes\FileHandler;

use App\Classes\Exceptions\FileException;

class Json extends AbstractFileHandler
{
    /**
     * @throws FileException
     */
    public function parseFile(string $filePath) :array
    {
        $json = file_get_contents($filePath);
        $result = json_decode($json, true);

        if (!$result) {
            $message = 'Invalid file structure: ' . json_last_error_msg();
            throw new FileException($message);
        }

        $indexArray = $this->toIndexArray($result);

        return $indexArray;
    }

    public function saveAsFile(string $filePath, array $array) :void
    {
        $result = json_encode($array);

        file_put_contents($filePath, $result);
    }

    private function toIndexArray(array $array) :array
    {
        return $this->isAssocArray($array) ? [$array] : $array;
    }
}
