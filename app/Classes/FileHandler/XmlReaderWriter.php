<?php

namespace App\Classes\FileHandler;

use XMLReader;
use XMLWriter;
use App\Classes\Exceptions\FileException;

class XmlReaderWriter extends AbstractFileHandler
{
    /**
     * @throws FileException
     */
    public function parseFile(string $filePath) :array
    {
        try {
            // Create XMLReader object and open xml file
            $reader = new XMLReader();
            $reader->open($filePath);
            // Convert Reader Object to array
            $assocArray = $this->xmlReaderToArray($reader);
        } catch (\Exception $e) {
            $message = 'Invalid file structure';
            throw new FileException($message);
        }

        $indexArray = $this->toIndexArray($assocArray);

        return $indexArray;
    }

    public function saveAsFile(string $filePath, array $array) :void
    {
        $result = $this->arrayToXmlWriter($array);

        file_put_contents($filePath, $result);
    }

    private function toIndexArray(array $array): array
    {
        if (count($array) === 1) {
            $array = array_first($array);
        }
        if (count($array) === 1) {
            $array = array_first($array);
        }

        $array = $this->isAssocArray($array) ? [$array] : $array;

        $result = [];
        foreach ($array as $key => $item) {
            $newItem = $item;
            if (isset($item['currency']) && $this->isAssocArray($item['currency'])) {
                $newItem['currency'] = [$item['currency']];
            }
            $result[] = $newItem;
        }

        return $result;
    }

    private function xmlReaderToArray(XMLReader $xml, string $name = null) :?array
    {
        if ($name === false) {
            $xml->read();
        }

        $tree = null;

        while ($xml->read()) {
            if ($xml->nodeType == XMLReader::END_ELEMENT) {
                return $tree;
            } elseif ($xml->nodeType == XMLReader::ELEMENT) {
                if ((!$xml->isEmptyElement)) {
                    $children = $this->xmlReaderToArray($xml, $xml->localName);
                    if (is_array($children) && count($children) > 1) {
                        if (isset($tree[$xml->localName])) {
                            if (!isset($tree[$xml->localName][0])) {
                                $moved = $tree[$xml->localName];
                                unset($tree[$xml->localName]);
                                $tree[$xml->localName][0] = $moved;
                            }
                            $tree[$xml->localName][] = $children;
                        } else {
                            $tree[$xml->localName] = $children;
                        }
                    } else {
                        $tree[$xml->localName] = $children;
                    }
                }
            }
        }

        return $tree;
    }

    private function arrayToXmlWriter(array $array, XMLWriter $xml = null, string $rootElement = null) :string
    {
        // Create new $xml object if we don't have parent element (name parent element)
        if ($rootElement === null) {
            $xml = new XMLWriter();
            $xml->openMemory();
            $xml->startDocument('1.0');
            $xml->startElement('root');
        }

        foreach ($array as $key => $value) {
            $newKey = $key;
            if (is_array($value)) { //nested array
                if (is_numeric($newKey) && $rootElement === null) {
                    $newKey = 'item';
                }
                if (is_numeric($newKey)) {
                    $newKey = 'currency';
                }
                $xml->startElement($newKey);
                $this->arrayToXmlWriter($value, $xml, $newKey);
                $xml->endElement();
            } else {
                $xml->writeElement($newKey, $value);
            }
        }

        // Close $xml object if we don't have parent element
        if ($rootElement === null) {
            $xml->endElement();
            $xml->endDocument();
            $xmlString = $xml->outputMemory(true);
            $xml->flush();
        }

        return $xmlString ?? '';
    }
}
