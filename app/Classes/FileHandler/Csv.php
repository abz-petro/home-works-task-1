<?php

namespace App\Classes\FileHandler;

class Csv extends AbstractFileHandler
{
    /**
     * @var string
     */
    private $csvSeparator = ',';

    public function parseFile(string $filePath) :array
    {
        $handle = fopen($filePath, 'r');
        $result = [];

        while (($data = fgetcsv($handle, 0, $this->csvSeparator)) !== false) {
            if ([null] !== $data) {
                $result[] = $data;
            }
        }

        $indexArray = $this->toIndexArray($result);

        return $indexArray;
    }

    public function saveAsFile(string $filePath, array $array) :void
    {
        $file = fopen($filePath, 'w');

        foreach ($array as $key => $item) {
            // Write date
            fputcsv($file, [$item['last_update']], $this->csvSeparator);
            // Write currency row
            foreach ($item['currency'] as $currency) {
                fputcsv($file, $currency, $this->csvSeparator);
            }
        }

        fclose($file);
    }

    public function toIndexArray(array $array) :array
    {
        $result = [];
        $newItem = [];

        foreach ($array as $item) {
            if (count($item) === 1) {
                if (isset($newItem['last_update']) && isset($newItem['currency'])) {
                    $result[] = $newItem;
                    $newItem = [];
                }
                $newItem['last_update'] = $item[0];
            } elseif (count($item) === 6 && isset($newItem['last_update'])) {
                $newItem['currency'][] = [
                    'name' => $item[0],
                    'unit' => $item[1],
                    'currencycode' => $item[2],
                    'country' => $item[3],
                    'rate' => $item[4],
                    'change' => $item[5],
                ];
            }
        }

        if (isset($newItem['last_update']) && isset($newItem['currency'])) {
            $result[] = $newItem;
        }

        return $result;
    }
}
